# wai - Where Am I

`wai` is a simple web application implementing a REST API to allow setting and
querying of where an individual is working today and when they will next be
available at a location.  The basic use case is to indicate when a user is
working from home and when they'll next be on campus or in the office.

## Example build and run

To build and run:

```
make && dist/wai_osx_x86_64 -D whatever://postgresql -p 1010
```

Of course, use the appropriate binary.

To use it, assuming user ID is in the shell variable `$USERID`:

```
$ curl localhost:1010/users/${USERID}
{"location":"Remote","status":"Just chillin","status_emoji":""}
$ curl -X PUT -F "location=On campus"  localhost:1010/users/${USERID}
{"message":"missing key in request header"}
$ curl -X PUT -H "X-Api-Key: valid-key" -F "location=On campus" localhost:1010/users/${USERID}
{"location":"On campus","status":"","status_emoji":""}
$ curl localhost:1010/users/${USERID}
{"location":"On campus","status":"","status_emoji":""}
```

Or something like that.  Test scriptage provides examples: give
`tests/basic.sh` a look.

## Web server

The web server provides API access (at `/api/`), as well as a homepage
describing the project (`/`), and embeddable HTML snippets for a user's status
(`/users/`).

### Embedding user info in a web page

To embed your location information and status in a web page, use the `embed`
HTML tag (`iframe` and `object` may also be used although have different
syntax and possibly behaviour).  For example:

```
<embed type="text/html" src='http://localhost:5000/users/${USERID}' />
```

Assuming the user is valid (of course, using a shell variable is not valid
syntax), this will embed their status, which will be something like:

```
<div id='wai-user-info'>
  <dl>
    <dt>Location</dt>
    <dd>In the office</dd>

    <dt>Status</dt>
    <dd>:emoji: Feelin' awesome</dd>
  </dl>
</div>
```

Embedded resources are not subject to local stylesheets, so to style this with
a different font family or layout, it is possible to specify a `style` query
parameter:

```
<embed type='text/html'
src='http://localhost:5000/users/${USERID}?style=dl%7Bfont-family%3Asans-serif%7D'>
```

The above uses a URL-encoded style string:

```
dl{font-family:sans-serif}
```

More elaborate stylings are possible but must be
[URL-encoded](https://www.w3schools.com/tags/ref_urlencode.asp).

## API

Everything you need to know about this complex and powerful API. :D

The examples given are using [Curl](https://curl.se), a powerful, flexible and
widely available command-line web client, but other tools may be used.

Examples are also available in `tests/basic.sh` which is how the application
is tested.

### User IDs

User IDs are provided by the system and are UUIDs, with the intention they are
unique and unguessable.  Of course, if you embed your status in a public web
page, it won't take a particularly clever person to figure out your UUID and,
I don't know, stick it on their own webpage for some odd reason.  But if you
use a decent API key, they shouldn't be able to change it.

### Register a new user

The first thing to do is to register a new user.  The Curl invocation for this
is:

```
$ curl -X POST -F "apikey=best key ever" http://localhost:5000/users/)
```

This establishes a new user with the given API key, which is used thereafter
to update status information.  The response contains the new user's ID and
will be used in subsequent updates and queries.

### Update status

To update the status, something like the following can be used:

```
curl -X PATCH -H "X-API-KEY: best key ever" \
  -F "location=Working remotely" -F "status=Present" \
  -F "status_emoji=:greencircle:" http://localhost:5000/users/$uuid
```

In this update we are setting the location, status and a status emoji showing
a green circle (to represent a green light).

Any of these can be cleared by updating with an empty string.  Not all fields
must be specified on an update, but only the fields that are specified are
updated.

### Query status

To query is simple:

```
curl http://localhost:5000/users/$uuid
```

This returns a JSON message with the current information.

## Development

### Set up databasae

You can use the provided Postgres container composition to run a local
Postgres instance:

```
$ TAG=wai docker-compose -f dev/docker-pgsql.yml up
```

Once it's up, you can log in and then initialize the dev role and database:

```
$ psql -h localhost -U postgres
postgres=# create user wai with password 'wai oh wai';
CREATE ROLE
postgres=# create database wai with owner=wai;
CREATE DATABASE
postgres=# \q
$
```

Now initialize the database with the schema:

```
$ psql postgres://wai:wai%20oh%20wai@localhost/wai < wai.psql
NOTICE:  table "schemalog" does not exist, skipping
DROP TABLE
NOTICE:  table "users" does not exist, skipping
DROP TABLE
NOTICE:  table "userkeys" does not exist, skipping
DROP TABLE
CREATE TABLE
INSERT 0 1
CREATE TABLE
CREATE TABLE
```

