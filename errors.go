package main

import (
	"log"
	"os"
)

func check(ec int, e error) {
	if e != nil {
		log.Fatal(e)
		os.Exit(ec)
	}
}

func debug(s string, a ...interface{}) {
	//log.Printf(s, a...)
}

func info(s string, a ...interface{}) {
	log.Printf(s, a...)
}

func warn(s string, a ...interface{}) {
	log.Printf("WARNING: "+s, a...)
}

func fatal(ec int, s string, a ...interface{}) {
	log.Printf("FATAL: "+s, a...)
	os.Exit(ec)
}
