SOURCE_DIR	:= .
OUTPUT_DIR	:= dist

version_go	:= $(SOURCE_DIR)/version.go
api_src		:= $(SOURCE_DIR)/api/openapi.yaml
api_go		:= $(SOURCE_DIR)/api.gen.go
api_templates	:= $(SOURCE_DIR)/api/*.tmpl
SOURCES		:= $(SOURCE_DIR)/*.go $(api_go)

prog_x86_64	:= $(OUTPUT_DIR)/wai_linux_x86-64
prog_osx_x86_64	:= $(OUTPUT_DIR)/wai_osx_x86_64
prog_osx_arm64	:= $(OUTPUT_DIR)/wai_osx_arm64

#ALL_BUILDS	:= $(prog_x86_64) $(prog_osx_x86_64) $(prog_osx_arm64)
ALL_BUILDS	:= $(prog_x86_64) $(prog_osx_x86_64)

# build flags
LD_FLAGS_COMMON	:= -w -s
LD_FLAGS	:= $(LD_FLAGS_COMMON)
LD_FLAGS_LINUX	:= -extldflags \"-static\" $(LD_FLAGS)

# determine version
VERSION		:= $(shell git describe 2>/dev/null | sed -E 's/[0-9]+-g([a-f0-9]{7})/dev-\1/g')

all: $(ALL_BUILDS)

$(version_go): .git/refs/heads
	@echo "package main\n\nconst PROGRAM_VERSION = \"$(VERSION)\"" > $@

$(api_go): $(api_src) $(api_templates)
	#oapi-codegen -generate types,server -package main $(api_src) > $(api_go)
	oapi-codegen -generate types,server -templates $(SOURCE_DIR)/api -package main $(api_src) > $(api_go)

$(prog_osx_x86_64): $(SOURCES) $(version_go)
	CGO_ENABLED=0 GOARCH=amd64 GOOS=darwin go build \
		-ldflags "$(LD_FLAGS)" -o $@

$(prog_x86_64):	$(SOURCES) $(version_go)
	CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build \
		-ldflags "$(LD_FLAGS_LINUX)" -o $@

$(prog_osx_arm64): $(SOURCES) $(version_go)
	CGO_ENABLED=0 GOARCH=arm64 GOOS=darwin go build \
		-ldflags "$(LD_FLAGS)" -o $@

wai: $(SOURCES)
	@go build wai.go

container: $(prog_x86_64) Dockerfile
	@docker build -t dlek/wai:$(VERSION) -t dlek/wai:latest .
	@docker push dlek/wai:$(VERSION)
	@docker push dlek/wai:latest

test: $(ALL_BUILDS)
	@./test.sh
