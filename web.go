/* Okay so this is for displaying little cards of Wai info in HTML, for
 * clients unable or unwilling to use Javascript.
 */
package main

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"html/template"
	"io"
	"net/http"
)

// --------------------------------------------------------------------------
//                                                                 globals
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
//                                                                 constants
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
//                                                               templating
// --------------------------------------------------------------------------

type Template struct {
	templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, ctx echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

// --------------------------------------------------------------------------
//                                                              web handler
// --------------------------------------------------------------------------

type WebHandlerIfc interface {
	GetHomepage(ctx echo.Context) error
	GetUserInfo(ctx echo.Context, id Userid) error
}

type WebHandler struct {
	templates *Template
	info      struct {
		Title string
		ServerUrl string
		APIUrl string
	}
}

func (wh *WebHandler) Init(templates string) error {
	debug("Initializing web handler")

	// initialize template engine
	wh.templates = &Template{
		templates: template.Must(template.ParseGlob(templates)),
	}
	if wh.templates == nil {
		warn("Could not initialize templates")
	}

	// initialize info struct
	wh.info.Title = "Wai"
	// TODO: use public name of service if possible
	wh.info.ServerUrl = "https://wai.example.org"
	wh.info.APIUrl = "https://wai.example.org/api"

	return nil
}

func (wh *WebHandler) Close() error {
	debug("Shutting down web handler")
	return nil
}

func (wh *WebHandler) Homepage(ctx echo.Context) error {
	return ctx.Render(http.StatusOK, "homepage", wh.info)
}

// TODO: allow client to specify or describe a template.  It's awkward to
// describe a template using GET, expressed as a query string, but is it
// reasonable to POST without updating, and to expect a translation in reply?
func (wh *WebHandler) GetUserInfo(ctx echo.Context) error {

	type Info struct {
		User                // use user struct
		Style *template.CSS `query:"style"`
	}
	var userinfo Info

	// get user ID and style parameter
	// TODO: if this doesn't work, check api.gen.go for the following
	// definition in the User struct:
	// Id *Userid `json:"id,omitempty" param:"userid"`
	err := ctx.Bind(&userinfo)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("Invalid format for parameter userid: %s", err))
	}
	info("Looking up user %s", *userinfo.Id)

	// convert to UUID
	uuid, err := uuid.Parse(string(*userinfo.Id))
	if err != nil {
		warn("Could not parse UUID %s: %v", *userinfo.Id, err)
		return ctx.Render(http.StatusBadRequest, "code400", wh.info)
	}

	// retrieve from database
	// TODO: copied from wai.go; generalize
	rows, err := Dbconn.Query(context.Background(), SQL_GET_INFO_BY_ID, uuid)
	if err != nil {
		warn("Could not lookup user %s", uuid)
		return ctx.Render(http.StatusInternalServerError, "code500", wh.info)
	}
	defer rows.Close()
	if !rows.Next() {
		return ctx.Render(http.StatusNotFound, "code404", wh.info)
	}

	// read into info object
	err = rows.Scan(&userinfo.Location, &userinfo.Status, &userinfo.StatusEmoji)
	if err != nil {
		info("Could not retrieve user by id %s: %v", userinfo.Id, err)
		return err
	}

	return ctx.Render(http.StatusOK, "userinfo", userinfo)
}

func RegisterWebHandlers(router EchoRouter, wh *WebHandler) {
	// route for displaying informational page about service
	router.GET("/", wh.Homepage)
	router.GET("/users/:userid", wh.GetUserInfo)
}
