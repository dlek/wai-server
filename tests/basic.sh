#!/bin/sh

CURL="curl -s"
URL=localhost:5000/api

try() {
  trying="$@"
  echo "Trying: $trying"
}

fail() {
  rc=$?
  echo "Failed (ec $rc): ${trying}"
  if [ -n "$1" ]
  then
    echo "Additional:"
    echo "$@"
  fi
  exit $rc
}

# ---------------------------------------------------------------------------
try simple condition

true || fail "Could not do the thing"

# ---------------------------------------------------------------------------
try registering new user

results=$($CURL -X POST -F "apikey=best key ever" $URL/users/) || fail
uuid=$(echo "$results" | sed -n 's/{"\(id\)":"\([0-9a-f]\{8\}-[0-9a-f]\{4\}-[0-9a-f]\{4\}-[0-9a-f]\{4\}-[0-9a-f]\{12\}\)"}/\2/p')
if [ -z "$uuid" ]
then
  fail "$results"
fi

# ---------------------------------------------------------------------------
try simple query with new user but no info

results=$($CURL $URL/users/$uuid ) || fail 
if [ "$results" != '{"location":"","status":"","status_emoji":""}' ]
then
  fail "$results"
fi

# ---------------------------------------------------------------------------
try authenticated update for new user

results=$($CURL -X PATCH -H "X-API-KEY: best key ever" -F "location=Working remotely" -F "status=Present" $URL/users/$uuid) || fail
if [ "$results" != '{"location":"Working remotely","status":"Present","status_emoji":""}' ]
then
  fail "$results"
fi

# ---------------------------------------------------------------------------
try simple query with new user for updated info

results=$($CURL $URL/users/$uuid ) || fail 
if [ "$results" != '{"location":"Working remotely","status":"Present","status_emoji":""}' ]
then
  fail "$results"
fi

# ---------------------------------------------------------------------------
try authenticated partial update

results=$($CURL -X PATCH -H "X-API-KEY: best key ever" -F "status=Not present" $URL/users/$uuid) || fail
if [ "$results" != '{"location":"Working remotely","status":"Not present","status_emoji":""}' ]
then
  fail "$results"
fi

# ---------------------------------------------------------------------------
try blanking a field

results=$($CURL -X PATCH -H "X-API-KEY: best key ever" -F "location=" $URL/users/$uuid) || fail
if [ "$results" != '{"location":"","status":"Not present","status_emoji":""}' ]
then
  fail "$results"
fi

# ---------------------------------------------------------------------------
try unauthenticated update for new user

results=$($CURL -X PATCH -H "X-API-KEY: wrongest key ever" -F "location=Working remotely" $URL/users/$uuid) || fail
if [ "$results" != '{"message":"Forbidden"}' ]
then
  fail "$results"
fi

# ---------------------------------------------------------------------------
echo Testing complete
