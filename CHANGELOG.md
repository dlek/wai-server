# Changelog

## v0.2.2 (20230205) Useful and informative homepage

Homepage of service contains documentation and tool for testing and
URL-encoding snippet style.

## v0.2.1 (20230205) Build fix

Added HTML templates to container image

## v0.2 (20230205) Add web frontend

Added web frontend for embedding HTML snippets in webpages without Javascript.

## v0.1 (20220308) Basic functionality

API and a database backend.
