package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/labstack/echo/v4"
	"net/http"
	"strings"
)

// --------------------------------------------------------------------------
//                                                                globals
// --------------------------------------------------------------------------

var Dbconn *pgxpool.Pool

// --------------------------------------------------------------------------
//                                                                constants
// --------------------------------------------------------------------------

// SQL strings
const SQL_GET_KEY_BY_ID = "SELECT key FROM userkeys WHERE user_id=$1"
const SQL_GET_INFO_BY_ID = "SELECT location, status, emoji FROM users WHERE id=$1"
const SQL_INSERT_NEW = "INSERT INTO USERS(id, location, status, emoji) VALUES($1, $2, $3, $4)"
const SQL_INSERT_NEW_KEY = "INSERT INTO USERKEYS(user_id, key) VALUES($1, $2)"

// error codes
const ERR_INVOCATION = 128
const ERR_CONFIGURATION = 129
const ERR_DB_INIT = 130
const ERR_WEB_INIT = 131

// program details
const PROGRAM_NAME = "wai"
const PROGRAM_USAGE = `
Usage: wai -help         for this help text, or
       wai [options]

Where Am I server.

Options:
`

// --------------------------------------------------------------------------
//                                                                structures
// --------------------------------------------------------------------------

type Client struct {
	// undefined for now
}

type Resource struct {
	Paths   []string
	Op      string
	From    string
	Command []string
	Script  string
}

type Config struct {
	Clients   []Client
	Resources []Resource
}

type Args struct {
	db_uri string
	hostip string
	port   int
}

// --------------------------------------------------------------------------
//                                                                functions
// --------------------------------------------------------------------------

// Parse arguments and handle usage display.  Return struct describing the
// arguments.
func parse_cli() Args {
	var args Args

	//config_fn := PROGRAM_NAME + ".yaml"
	//flag.StringVar(&args.config_fn, "config", config_fn, "config file")

	flag.StringVar(&args.db_uri, "D", "", "database URI")
	flag.StringVar(&args.hostip, "H", "", "hostname or IP to listen on")
	flag.IntVar(&args.port, "p", 5000, "listening port")

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "%s version %s", PROGRAM_NAME, PROGRAM_VERSION)
		fmt.Fprintf(flag.CommandLine.Output(), PROGRAM_USAGE)
		flag.PrintDefaults()
	}

	flag.Parse()
	return args
}

type Error struct {
	// Error code
	Code int32 `json:"code"`

	// Error message
	Message string `json:"message"`
}

type ServerImplementation struct {
	Users map[Userid]User
	Keys  map[Userid]Userkey
}

func (si ServerImplementation) Init(dsn string) error {
	info("Initializing database connection")
	var err error
	Dbconn, err = pgxpool.Connect(context.Background(), dsn)
	if err != nil {
		warn("Could not initialize database connection: %s", err)
	}
	return err
}

func (si ServerImplementation) Close() {
	info("Closing database connection")
	Dbconn.Close()
}

func (si ServerImplementation) ApiKeyAuth(ctx echo.Context) (int, string) {
	var given string
	var correct string
	if apikeys, ok := ctx.Request().Header["X-Api-Key"]; ok {
		given = apikeys[0]
	} else {
		return http.StatusUnauthorized, "Authorization required"
	}

	id := ctx.Param("id")
	if id == "" {
		// authentication required
		return http.StatusUnauthorized, "Provide \"id\" field"
	}

	rows, err := Dbconn.Query(context.Background(),
		SQL_GET_KEY_BY_ID, id)
	if err != nil {
		// TODO: should this really exit
		fatal(167, "Failure on query: %v", err)
	}
	defer rows.Close()

	if !rows.Next() {
		info("Could not find key for uuid %v", id)
		return http.StatusUnauthorized, "ID not recognized"
	}
	err = rows.Scan(&correct)
	if err != nil {
		warn("http.StatusUnauthorized but not sure why")
		return http.StatusUnauthorized, "Not sure why"
	}

	if given != correct {
		info("Failed API key authorization when given ('%s') != correct ('%s')", given, correct)
		return http.StatusForbidden, "Forbidden"
	}
	info("User authenticated (%v)", id)
	return http.StatusOK, "OK"
}

func (si ServerImplementation) GetUserById(ctx echo.Context, id Userid) error {

	uuid, err := uuid.Parse(string(id))
	if err != nil {
		warn("Could not parse UUID %s: %v", id, err)
	}
	rows, err := Dbconn.Query(context.Background(), SQL_GET_INFO_BY_ID, uuid)
	if err != nil {
		info("Could not look up id %s", uuid)
	}
	defer rows.Close()

	if !rows.Next() {
		return ctx.JSON(http.StatusNotFound, Error{
			Code:    http.StatusNotFound,
			Message: "Not found"})
	}
	var location string
	var status string
	var emoji string
	err = rows.Scan(&location, &status, &emoji)
	if err != nil {
		info("Could not retrieve user by id %s: %v", id, err)
		return err
	}
	user := User{
		Status:      &status,
		StatusEmoji: &emoji,
		Location:    &location}
	return ctx.JSON(http.StatusOK, user)
}

func (si ServerImplementation) UpdateUserById(ctx echo.Context, id Userid) error {

	info("UpdateUserById(id %v)", id)

	uuid, err := uuid.Parse(string(id))
	if err != nil {
		warn("Could not parse UUID %s: %v", id, err)
	}

	// parse form
	form, _ := ctx.FormParams()

	// TODO: is there a simpler way of doing this :(
	var terms []interface{}
	var queryterms []string
	i := 0
	if form["location"] != nil {
		location := ctx.FormValue("location")
		terms = append(terms, location)
		queryterms = append(queryterms, fmt.Sprintf("location=$%d", i+1))
		i++
	}

	if form["status"] != nil {
		status := ctx.FormValue("status")
		terms = append(terms, status)
		queryterms = append(queryterms, fmt.Sprintf("status=$%d", i+1))
		i++
	}

	if form["status_emoji"] != nil {
		status_emoji := ctx.FormValue("status_emoji")
		terms = append(terms, status_emoji)
		queryterms = append(queryterms, fmt.Sprintf("emoji=$%d", i+1))
		i++
	}

	querystr := fmt.Sprintf("UPDATE users SET %s WHERE id=$%d", strings.Join(queryterms, ", "), i+1)
	terms = append(terms, uuid)
	_, err = Dbconn.Exec(context.Background(), querystr, terms...)
	if err != nil {
		warn("Could not update user info: %v", err)
		return err
	}

	return si.GetUserById(ctx, id)
}

type Registration struct {
	Id *Userid `json:"id"`
}

// this ridiculousness is just so we can maintain NULL values if the request
// does not contain all the fields, instead of inserting empty strings into
// the table
func nonempty(str *string) *string {
	if *str == "" {
		return nil
	}
	return str
}

func (si ServerImplementation) RegisterUser(ctx echo.Context) error {
	info("Registering new user")

	// generate user ID
	user_id := Userid(fmt.Sprintf("%s", uuid.New()))

	// retrieve submitted values
	location := ctx.FormValue("location")
	status := ctx.FormValue("status")
	status_emoji := ctx.FormValue("status_emoji")
	apikey := ctx.FormValue("apikey")

	/* create objects, but actually we can just put everything in database and
	 * forget
	 * ...except we want to return a user object at the end
	 */
	user := User{}
	user.Id = &user_id
	if location != "" {
		user.Location = &location
	}
	if status != "" {
		user.Status = &status
	}
	if status_emoji != "" {
		user.StatusEmoji = &status_emoji
	}

	/* don't need userkey object though
	  -------------------------------------------------------------------------------
		userkey := Userkey{}
		userkey.Apikey = &apikey
	  userkey.UserId = &user_id
	  -------------------------------------------------------------------------------
	*/

	// should userinfo and userkey be merged??  Just delete apikey field before
	// returning, or otherwise omit it?

	uuid, err := uuid.Parse(string(user_id))
	if err != nil {
		return err
	}

	// check database connection
	if Dbconn == nil {
		fatal(166, "Null database connection!")
	}

	// insert user record
	_, err = Dbconn.Exec(context.Background(),
		SQL_INSERT_NEW,
		uuid, location, status, status_emoji)
	if err != nil {
		warn("Could not insert user: %v", err)
		return err
	}

	// insert user key
	_, err = Dbconn.Exec(context.Background(),
		SQL_INSERT_NEW_KEY,
		uuid, apikey)
	if err != nil {
		warn("Could not insert user key: %v", err)
		return err
	}

	// return user object
	return ctx.JSON(http.StatusOK, user)
}

// --------------------------------------------------------------------------
//                                                                main
// --------------------------------------------------------------------------

func main() {

	// get flags - first thing in case of invocation error or usage
	args := parse_cli()

	info("Starting " + PROGRAM_NAME + " (version " + PROGRAM_VERSION + ")")
	debug("Using database %s", args.db_uri)

	// determine listening address
	address := fmt.Sprintf("%s:%d", args.hostip, args.port)
	info("Listening on address %s", address)

	e := echo.New()
	api := ServerImplementation{}
	err := api.Init(args.db_uri)
	if err != nil {
		fatal(ERR_DB_INIT, "Could not initialize database")
	}
	defer api.Close()

	RegisterHandlersWithBaseURL(e, &api, "/api")

	web := WebHandler{}
	err = web.Init("html/*.html")
	if err != nil {
		fatal(ERR_WEB_INIT, "Cannot initialize web handler")
	}
	defer web.Close()
	RegisterWebHandlers(e, &web)
	e.Renderer = web.templates
	e.Static("/static", "html/static")

	e.Logger.Fatal(e.Start(address))
}
