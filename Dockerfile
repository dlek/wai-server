FROM golang:1.17-bullseye as build

WORKDIR /go/src/wai
ADD . /go/src/wai

RUN go get -d -v ./...

RUN go build -o /go/bin/wai

FROM gcr.io/distroless/base-debian11
COPY --from=build /go/bin/wai /
COPY --from=build /go/src/wai/html /html
CMD ["/wai"]
